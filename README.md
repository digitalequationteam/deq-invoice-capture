# deq-invoice-capture

Captures a Digital Equation invoice URL as a full-page PNG image.

### Options

```
  -h, --help            Display this usage guide.
  -u, --url <url>       Invoice URL to capture.
  -o, --output <path>   Output path to use for image generation.
```

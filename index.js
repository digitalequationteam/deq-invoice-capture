const
puppeteer        = require('puppeteer'),
commandLineArgs  = require('command-line-args'),
commandLineUsage = require('command-line-usage');

const optionDefinitions = [
    {
        name       : 'help',
        alias      : 'h',
        type       : Boolean,
        description: 'Display this usage guide.'
    }, {
        name       : 'url',
        alias      : 'u',
        type       : String,
        description: 'Invoice URL to capture.',
        typeLabel  : '<url>'
    }, {
        name       : 'output',
        alias      : 'o',
        type       : String,
        description: 'Output path to use for image generation.',
        typeLabel  : '<path>'
    }
];

options = commandLineArgs(optionDefinitions);

let app = {
    help: function() {
        const usage = commandLineUsage([
            {
                header : 'deq-invoice-capture',
                content: 'Captures a Digital Equation invoice URL as a full-page PNG image.'
            }, {
                header    : 'Options',
                optionList: optionDefinitions
            }, {
                content: 'Project home: [underline]{https://bitbucket.org/digitalequationteam/deq-invoice-capture/overview}'
            }
        ]);

        console.log(usage);
    },

    process: function(url, output) {
        (async () => {
            const browser = await puppeteer.launch({
                args: [
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--disable-dev-shm-usage'
                ]
            });

            const page = await browser.newPage();

            page.on('requestfailed', () => {
                console.log('ERROR: The invoice page request has failed or timed out!');
                process.exit(1);
            });
            page.on('error', () => {
                console.log('ERROR: The requested invoice page has crashed!');
                process.exit(2);
            });

            page.setViewport({
                width            : 1000,
                height           : 800,
                deviceScaleFactor: 2
            });

            try {
                await page.goto(url);
            } catch(e) {
                console.log('ERROR: The requested invoice page could not be accessed!');
                process.exit(3);
            }

            try {
                await page.waitForFunction('GLOBALS.pdfReady === true', { timeout: 15000 });
            } catch(e) {
                console.log('ERROR: Invoice global ready state could not be evaluated!');
                process.exit(4);
            }

            // await page.waitFor(3000);

            // await page.emulateMedia('screen');
            // await page.pdf({
            //     path  : output,
            //     format: 'Letter',
            //     margin: {
            //         top   : '0.4in',
            //         right : '0.4in',
            //         bottom: '0.4in',
            //         left  : '0.4in'
            //     }
            // });

            await page.screenshot({
                path    : output,
                fullPage: true
            });

            await browser.close();
            process.exit(0);
        })();
    }
};

if (options.help || !options.url || !options.output) {
    app.help();
} else {
    app.process(options.url, options.output);
}

const URL = 'http://deq-core.local/#/payment_pdf/6621af9b345dd184722c71d196d53a2691fdb9b6/3/9/invoice';
